extern crate collision_detection;
extern crate serde;
extern crate serde_json;

use collision_detection::{Path, Rectangle, Point, ensure_no_collision};

fn get_fixtures() -> Vec<Path> {
    let area_parachuts = Path {
        id : 1,
        description : "Skoki spadochronowe".to_string(),
        rectangles : vec![
            Rectangle {
                left_top : Point { x : 0, y : 10 },
                right_bottom : Point { x : 10, y : 0 },
            },
        ],
        restricted_aircrafts : vec![
            "SP-PAR".to_string(),
        ],
    };

    let area_airport = Path {
        id : 2,
        description : "Lotnisko".to_string(),
        rectangles : vec![
            Rectangle {
                left_top : Point { x : 30, y : 10 },
                right_bottom : Point { x : 40, y : 0 },
            },
        ],
        restricted_aircrafts : vec![],
    };

    let area_army = Path {
        id : 3,
        description : "Cwiczenia wojskowe".to_string(),
        rectangles : vec![
            Rectangle {
                left_top : Point { x : 50, y : 100 },
                right_bottom : Point { x : 100, y : 20 },
            },
        ],
        restricted_aircrafts : vec![
            "SP-MI1".to_string(),
            "SP-MI2".to_string(),
            "SP-MI3".to_string(),
        ],
    };

    // Serialize it to a JSON string.
    let j = serde_json::to_string(&area_army).unwrap();

    // Print, write to a file, or send to an HTTP server.
    println!("area_army {}", j);

    let autonomous_dron_path_1 = Path {
        id : 4,
        description : "Dron zwiadowca".to_string(),
        rectangles : vec![
            Rectangle {
                left_top : Point { x : 5, y : 115 },
                right_bottom : Point { x : 25, y : 85 },
            },
            Rectangle {
                left_top : Point { x : 5, y : 135 },
                right_bottom : Point { x : 65, y : 115 },
            },
            Rectangle {
                left_top : Point { x : 65, y : 155 },
                right_bottom : Point { x : 85, y : 115 },
            },
        ],
        restricted_aircrafts : vec![
            "SP-DR1".to_string(),
        ],
    };

    let autonomous_dron_path_2 = Path {
        id : 5,
        description : "Przewoz pizzy".to_string(),
        rectangles : vec![
            Rectangle {
                left_top : Point { x : 145, y : 45 },
                right_bottom : Point { x : 155, y : 20 },
            },
            Rectangle {
                left_top : Point { x : 155, y : 45 },
                right_bottom : Point { x : 170, y : 35 },
            },
            Rectangle {
                left_top : Point { x : 170, y : 75 },
                right_bottom : Point { x : 180, y : 35 },
            },
            Rectangle {
                left_top : Point { x : 180, y : 75 },
                right_bottom : Point { x : 200, y : 65 },
            },
        ],
        restricted_aircrafts : vec![
            "SP-DR2".to_string(),
        ],
    };

    vec![area_parachuts, area_airport, area_army, autonomous_dron_path_1, autonomous_dron_path_2]
}

#[test]
fn test_autonomous_dron_collision_1_with_dron_1() {
    let mut paths = get_fixtures();

    let autonomous_dron_collision_path = Path {
        id : 10,
        description : "Kurs kolizyjny z dronem 1".to_string(),
        rectangles : vec![
            Rectangle {
                left_top : Point { x : 35, y : 130 },
                right_bottom : Point { x : 40, y : 90 },
            },
        ],
        restricted_aircrafts : vec![
            "SP-DR-KOL".to_string(),
        ],
    };

    assert!(! ensure_no_collision(&mut paths, autonomous_dron_collision_path));
}


#[test]
fn test_autonomous_dron_collision_2_with_dron_1() {
    let mut paths = get_fixtures();

    let autonomous_dron_collision_path = Path {
        id : 10,
        description : "Kurs kolizyjny z dronem 1".to_string(),
        rectangles : vec![
            Rectangle {
                left_top : Point { x : 60, y : 150 },
                right_bottom : Point { x : 105, y : 145 },
            },
        ],
        restricted_aircrafts : vec![
            "SP-DR-KOL".to_string(),
        ],
    };

    assert!(! ensure_no_collision(&mut paths, autonomous_dron_collision_path));
}

#[test]
fn test_autonomous_dron_collision_3_with_dron_1() {
    let mut paths = get_fixtures();

    let autonomous_dron_collision_path = Path {
        id : 10,
        description : "Kurs kolizyjny z dronem 1".to_string(),
        rectangles : vec![
            Rectangle {
                left_top : Point { x : 75, y : 135 },
                right_bottom : Point { x : 80, y : 140 },
            },
        ],
        restricted_aircrafts : vec![
            "SP-DR-KOL".to_string(),
        ],
    };

    assert!(! ensure_no_collision(&mut paths, autonomous_dron_collision_path));
}

#[test]
fn test_autonomous_dron_collision_1_with_army_area() {
    let mut paths = get_fixtures();

    let autonomous_dron_collision_path = Path {
        id : 10,
        description : "Kurs kolizyjny z terenem wojska".to_string(),
        rectangles : vec![
            Rectangle {
                left_top : Point { x : 35, y : 60 },
                right_bottom : Point { x : 40, y : 40 },
            },
            Rectangle {
                left_top : Point { x : 40, y : 60 },
                right_bottom : Point { x : 60, y : 55 },
            },
            Rectangle {
                left_top : Point { x : 55, y : 55 },
                right_bottom : Point { x : 60, y : 45 },
            },
        ],
        restricted_aircrafts : vec![
            "SP-DR-KOL".to_string(),
        ],
    };

    assert!(! ensure_no_collision(&mut paths, autonomous_dron_collision_path));
}

#[test]
fn test_autonomous_dron_no_collision() {
    let mut paths = get_fixtures();

    let autonomous_dron_no_collision_path = Path {
        id : 10,
        description : "Kurs niekolizyjny".to_string(),
        rectangles : vec![
            Rectangle {
                left_top : Point { x : 200, y : 15 },
                right_bottom : Point { x : 225, y : 5 },
            },
            Rectangle {
                left_top : Point { x : 225, y : 25 },
                right_bottom : Point { x : 235, y : 5 },
            },
        ],
        restricted_aircrafts : vec![
            "SP-DR-NO-KOL".to_string(),
        ],
    };

    assert!(ensure_no_collision(&mut paths, autonomous_dron_no_collision_path));
}

#[test]
fn write_fixtures() {
    let mut paths = get_fixtures();

    // Serialize it to a JSON string.
    let j = serde_json::to_string(&paths).unwrap();

    // Print, write to a file, or send to an HTTP server.
    println!("paths {}", j);
}
