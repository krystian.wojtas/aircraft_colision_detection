extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Point {
    pub x: i32,
    pub y: i32,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Rectangle {
    pub left_top: Point,
    pub right_bottom: Point,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Path {
    pub id : i64,
    pub description: String,
    pub rectangles : Vec<Rectangle>,
    pub restricted_aircrafts : Vec<String>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Drone {
    pub id : i64,
    pub position : Point,
    pub flight_number : String,
}

pub fn ensure_no_collision(paths : &mut Vec<Path>, path_to_add : Path) -> bool {
    for ref path in paths.iter() {
        for ref rectangle in path.rectangles.iter() {
            for ref rectangle_to_add in path_to_add.rectangles.iter() {
                // println!("Rectangle {:?}", rectangle);
                if ! (
                    rectangle_to_add.left_top.x > rectangle.right_bottom.x ||
                    rectangle_to_add.right_bottom.x < rectangle.left_top.x ||
                    rectangle_to_add.left_top.y < rectangle.right_bottom.y ||
                    rectangle_to_add.right_bottom.y > rectangle.left_top.y
                ) {
                    // println!("Rectangle {:?}", rectangle);
                    // println!("Rectangle to add {:?}", rectangle_to_add);
                    return false;
                }
            }
        }
    }

    true
}
