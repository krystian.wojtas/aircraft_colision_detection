extern crate serde;
extern crate serde_json;
extern crate collision_detection;

// #[macro_use]
// extern crate serde_derive;

use std::env;
// use serde_json::Error;

use collision_detection::{Path, Rectangle, Point, Drone, ensure_no_collision};
// use collision_detection::*;
use std::io;
use std::io::Read;
use std::io::Write;
use std::fs::File;

enum ArgCliOperation {
    ArgCliOperationCreate,
    ArgCliOperationRemove,
}

enum ArgCliObject {
    ArgCliObjectArea,
    ArgCliObjectDrone,
}

use ArgCliOperation::*;
use ArgCliObject::*;

fn usage() -> String {
    let args: Vec<String> = env::args().collect();

    String::new() + "Usage: " + &args[0] + " create|delete area|drone|path"
}

fn parse_cli(args : &Vec<String>) -> (ArgCliOperation, ArgCliObject) {
    if args.len() != 3 &&
       args.len() != 4  {
        panic!(usage());
    }

    let arg_cli_operation = &args[1];
    let arg_cli_operation = match arg_cli_operation.as_ref() {
        "create" => Ok(ArgCliOperationCreate),
        "delete" => Ok(ArgCliOperationRemove),
        _ => Err(()),
    }.expect(&usage()[..]);

    let arg_cli_object = &args[2];
    let arg_cli_object = match arg_cli_object.as_ref() {
        "area" => Ok(ArgCliObjectArea),
        "drone" => Ok(ArgCliObjectDrone),
        _ => Err(()),
    }.expect(&usage()[..]);

    (arg_cli_operation, arg_cli_object)
}

fn read_paths() -> Vec<Path> {
    let mut file = File::open("paths.json").unwrap();
    let mut paths = String::new();
    file.read_to_string(&mut paths).unwrap();

    // Parse the string of data into a Person object. This is exactly the
    // same function as the one that produced serde_json::Value above, but
    // now we are asking it for a Person as output.
    let paths: Vec<Path> = serde_json::from_str(&paths).unwrap();
    paths
}

fn write_paths(paths: &mut Vec<Path>) {
    let mut file = File::create("paths.json").unwrap();

    // Serialize it to a JSON string.
    let j = serde_json::to_string(&paths).unwrap();

    // Print, write to a file, or send to an HTTP server.
    write!(file, "{}", j).unwrap();
}

fn read_area() -> Path {
    let mut path = String::new();
    io::stdin().read_to_string(&mut path).expect("Failed to read standard input");

    // Parse the string of data into a Person object. This is exactly the
    // same function as the one that produced serde_json::Value above, but
    // now we are asking it for a Person as output.
    let path: Path = serde_json::from_str(&path).unwrap();

let path2 = Path {
    rectangles : vec![ Rectangle { 
        left_top : Point {
            x : path.rectangles.get(0).unwrap().left_top.x * 1000,
            y : path.rectangles.get(0).unwrap().left_top.y * 1000
         },
         right_bottom : Point {
            x : path.rectangles.get(0).unwrap().right_bottom.x * 1000,
            y : path.rectangles.get(0).unwrap().right_bottom.y * 1000
         }
    } ],
    ..path
};

    path2
}

fn create_area(paths : &mut Vec<Path>) -> bool {
    println!("create area");

    let path_to_create = read_area();

    let mut paths2 = paths.clone();
    let path_to_create2 = path_to_create.clone();
    if ensure_no_collision(paths, path_to_create) {
        paths2.push(path_to_create2);
        write_paths(&mut paths2);
        true
    } else {
        false
    }
}

fn delete_area(paths : &mut Vec<Path>) -> bool {
    println!("delete area");

    let area_to_delete = read_area();

    let mut paths2 = paths.clone();
    paths2.retain(|ref path| path.id != area_to_delete.id );
    write_paths(&mut paths2);

    true
}

fn read_drone() -> Drone {
    let mut drone = String::new();
    io::stdin().read_to_string(&mut drone).expect("Failed to read standard input");

    // Parse the string of data into a Person object. This is exactly the
    // same function as the one that produced serde_json::Value above, but
    // now we are asking it for a Person as output.
    let drone: Drone = serde_json::from_str(&drone).unwrap();

    drone
}

fn create_drone(paths : &mut Vec<Path>) -> bool {
    println!("create drone");

    let drone = read_drone();
    let drone2 = drone.clone();
    let drone3 = drone.clone();

    let path_to_create = Path {
        // TODO ensure drone id is uniq among other fields in database
        id : drone.id,
        description : "Drone".to_string(),
        rectangles : vec![
            Rectangle {
                left_top : Point { x : drone.position.x - 1, y : drone.position.y + 1 },
                right_bottom : Point { x : drone.position.x + 1, y : drone.position.y - 1 },
            },
        ],
        restricted_aircrafts : vec![ drone.flight_number ],
    };

    let mut paths2 = paths.clone();
    let path_to_create2 = path_to_create.clone();
    // paths2.retain(|ref path| path.id != drone2.id );
println!("paths2 before {:?}", paths2);
    paths2.retain(|ref path| { 
println!("path {:?}", path);
println!("drone {:?}", drone3);
path.id != drone2.id } );
println!("paths2 after {:?}", paths2);

    if ensure_no_collision(&mut paths2, path_to_create) {
        paths2.push(path_to_create2);
        write_paths(&mut paths2);
        true
    } else {
        false
    }
}

fn delete_drone(paths : &mut Vec<Path>) -> bool {
    println!("delete drone");

    let drone_to_delete = read_drone();

    let mut paths2 = paths.clone();
    paths2.retain(|ref path| path.id != drone_to_delete.id );
    write_paths(&mut paths2);

    true
}

fn main() {
    let args: Vec<String> = env::args().collect();

    let (arg_cli_operation, arg_cli_object) = parse_cli(&args);

    let mut paths = read_paths();

    let result = match (arg_cli_operation, arg_cli_object) {
        (ArgCliOperationCreate, ArgCliObjectArea) => create_area(&mut paths),
        (ArgCliOperationRemove, ArgCliObjectArea) => delete_area(&mut paths),
        (ArgCliOperationCreate, ArgCliObjectDrone) => create_drone(&mut paths),
        (ArgCliOperationRemove, ArgCliObjectDrone) => delete_drone(&mut paths),
    };

    match result {
        true => {
            println!("Ok");
            std::process::exit(0);
        }
        false => {
            println!("Error");
            std::process::exit(1);
        }
    }
}
